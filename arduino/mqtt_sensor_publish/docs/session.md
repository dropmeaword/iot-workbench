## Session on IoT telemetry using a Wemos D1 mini pro

### Prepare

1. Get yourself a Wemos
2. In your Arduino environment, install the Wemos (ESP8266 development kit)
    - once the Arduino IDE has been opened you need to go into `File -> Preferences`
    - add the following url into the box labelled `Additional Boards Manager URLs`
    - http://arduino.esp8266.com/stable/package_esp8266com_index.json
3. Create an account on [demo.thingsboard.io](https://demo.thingsboard.io)
4. Create a device called `MDD data-gathering thing` and copy the access token for that device from the web interface

### Configure the code

1. In your Arduino environment, open the `config.h` file
2. You can configure two things here, the WiFi connection details and the MQTT broker details.
3. Configure the correct WiFi network name in the `wifi_ssid` field
4. Configure the correct WiFi password in the `wifi_password` field
5. Paste the access token you copied from thingsboard.io in the field labelled `mqtt_broker_token`

### Create your first **Internet of Things Thing**

1. Upload the code to your Wemos
2. Check in the `Serial Monitor` the the connection happens scuessfully and that the Wemos is sending data
3. If everything goes well, you should see that your Wemos sends one data sample every two seconds, and should be producing an output that looks somewhat like this:

```
{"temperature":14,"humidity":93}
{"temperature":15,"humidity":95}
{"temperature":10,"humidity":91}
{"temperature":12,"humidity":95}
{"temperature":13,"humidity":90}
{"temperature":12,"humidity":95}
{"temperature":15,"humidity":95}
{"temperature":11,"humidity":93}
{"temperature":13,"humidity":95}
{"temperature":14,"humidity":93}
{"temperature":15,"humidity":91}
```

### Looking at the data 

It's time to go to the server and fiddle with our thing there. Let's try and visualize the incoming data.

1. Go to `Dashboards` and add a new one using the round button on the bottom right
2. Let's call this one `MDD humidity + temperature`
3. Click on the pen icon to enter edit mode
4. Click on the plus button to add a new widget
5. Click on `Create a new widget`
6. Using the dropdown that appears on the right panel you should be able to chose among a broad selection of predefined widgets
7. For our exercise we will choose `Analogue gauges`, and pick the one you like best
8. A new modal window will appear asking us about the data source for this gauge
9. Add a new datasource of type `entity`, in the alias section type anything and a link will appear that will let you select `create a new one`
10. A new `add alias` pop up appears, call your alias whatever you like, in filter type select `single entity`, type = `device` and in the device box select the device you created above `MDD data-gathering thing`
11. now the two data-streams **humidity** and **temperature** will be available to the next field named `timeseries`, select the one you want to associate the widget with and click on the `add` button at the bottom of the dialog
12. that's it!

### Going the extra mile

1. Explore the posibilities of thingsboard.io dashboards, see what kinds of things you can create cool panels for
2. Add a sensor to your Wemos setup and try to pump the data that you gather from that sensor to your newly flanged dashboard
3. Add another sensor and make a thing that publishes more than one data stream


