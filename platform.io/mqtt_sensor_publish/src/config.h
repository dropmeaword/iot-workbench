#ifndef __CONFIG_H__
#define __CONFIG_H__

// these are the settings of your IoT wifi network
#define wifi_ssid      "your wifi network name"
#define wifi_password  "your wifi password"

// THINGSBOARD.IO CONFIG
// ///////////////////////////////////////////////
#define mqtt_broker "demo.thingsboard.io"
#define mqtt_broker_port 1883

#define mqtt_broker_token "WkAZ7LQmwUbdrXwiQ0KO"
#define mqtt_broker_token_key  NULL

// SHIFTR.IO CONFIG
// ///////////////////////////////////////////////
// // broker and access credentials
// #define mqtt_broker "broker.shiftr.io"
// #define mqtt_broker_port 1883

// // this you will have to create in shirt.io from your own account, they are called `tokens` there
// #define mqtt_broker_username "ae196cd0"
// #define mqtt_broker_password "1cea28cb46b561d1"


#define mqtt_pub_topic "v1/devices/me/telemetry"    // the mqtt topic your device will publish to
#define mqtt_sub_topic "v1/devices/ctrl"   // the mqtt topic your device will subscribe to

#endif // __CONFIG_H__
